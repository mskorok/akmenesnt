<?php

namespace Namas\ArcanaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\owner;
use Namas\ArcanaBundle\Form\ownerType;

/**
 * owner controller.
 *
 * @Route("/owner")
 */
class ownerController extends BaseController
{

    /**
     * Lists all owner entities.
     *
     * @Route("/", name="owner")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:owner')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );

        return array(
            'entities' => $pagination,
        );


    }

    /**
     * Creates a new owner entity.
     *
     * @Route("/", name="owner_create")
     * @Method("POST")
     * @Template("ArcanaBundle:owner:new.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $entity = new owner();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('owner_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a owner entity.
     *
     * @param owner $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(owner $entity)
    {
        $form = $this->createForm(new ownerType(), $entity, array(
            'action' => $this->generateUrl('owner_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new owner entity.
     *
     * @Route("/new", name="owner_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new owner();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a owner entity.
     *
     * @Route("/{id}", name="owner_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArcanaBundle:owner')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find owner entity.');
        }
        /** @var \Namas\ArcanaBundle\Entity\entity $entity */
        $entity = $this->fillTranslatedEntity($entity);

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing owner entity.
     *
     * @Route("/{id}/edit", name="owner_edit")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var owner $entity */
        $entity = $em->getRepository('ArcanaBundle:owner')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find owner entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a owner entity.
    *
    * @param owner $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(owner $entity)
    {
        $form = $this->createForm(new ownerType(), $entity, array(
            'action' => $this->generateUrl('owner_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing owner entity.
     *
     * @Route("/{id}", name="owner_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:owner:edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var owner $entity */
        $entity = $em->getRepository('ArcanaBundle:owner')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find owner entity.');
        }

        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('owner_show', array('id' => $id)));
            //return $this->redirect($this->generateUrl('owner'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a owner entity.
     *
     * @Route("/{id}", name="owner_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ArcanaBundle:owner')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find owner entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('owner'));
    }

    /**
     * Creates a form to delete a owner entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('owner_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $entity = new owner();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-form')
        );

    }


}
