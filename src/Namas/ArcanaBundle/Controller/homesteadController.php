<?php

namespace Namas\ArcanaBundle\Controller;

use Namas\ArcanaBundle\Entity\entity;
use Namas\ArcanaBundle\Entity\image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\homestead;
use Namas\ArcanaBundle\Form\homesteadType;

/**
 * homestead controller.
 *
 * @Route("/homestead")
 */
class homesteadController extends BaseController
{

    /**
     * Lists all homestead entities.
     *
     * @Route("/", name="homestead")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:homestead')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination
        );
    }
    /**
     * Creates a new homestead entity.
     *
     * @Route("/", name="homestead_create")
     * @Method("POST")
     * @Template("ArcanaBundle:homestead:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new homestead();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('homestead_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a homestead entity.
     *
     * @param homestead $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(homestead $entity)
    {
        $form = $this->createForm(new homesteadType(), $entity, array(
            'action' => $this->generateUrl('homestead_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new homestead entity.
     *
     * @Route("/new", name="homestead_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new homestead();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a homestead entity.
     *
     * @Route("/{id}", name="homestead_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var homestead $entity */
        $entity = $em->getRepository('ArcanaBundle:homestead')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homestead entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);


        $images= $entity->getImages();
        $img = '';
        foreach($images as $image){
            /** @var string $img */
            $img .= '<img src="/uploads/images/'.$image->getPath().'" width="150" height="125" />';
        }
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img
        );
    }

    /**
     * Displays a form to edit an existing homestead entity.
     *
     * @Route("/{id}/edit", name="homestead_edit")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var homestead $entity */
        $entity = $em->getRepository('ArcanaBundle:homestead')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homestead entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a homestead entity.
    *
    * @param homestead $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(homestead $entity)
    {
        $form = $this->createForm(new homesteadType(), $entity, array(
            'action' => $this->generateUrl('homestead_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing homestead entity.
     *
     * @Route("/{id}", name="homestead_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:homestead:edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entity */
        $entity = $em->getRepository('ArcanaBundle:homestead')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homestead entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('homestead_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a homestead entity.
     *
     * @Route("/{id}", name="homestead_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var homestead $entity */
            $entity = $em->getRepository('ArcanaBundle:homestead')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find homestead entity.');
            }
            $images =$entity->getImages();
            /** @var image $image */
            foreach($images as $image){
                $em->remove($image);
                $em->flush();
            }
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('homestead'));
    }

    /**
     * Creates a form to delete a homestead entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('homestead_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $entity = new homestead();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-form')
        );

    }
}
