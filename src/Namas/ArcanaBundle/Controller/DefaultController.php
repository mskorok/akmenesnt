<?php

namespace Namas\ArcanaBundle\Controller;

use Namas\ArcanaBundle\Form\postType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ParameterBag;
use Namas\ArcanaBundle\Entity\apartment;
use Namas\ArcanaBundle\Entity\forest;
use Namas\ArcanaBundle\Entity\post;
use Namas\ArcanaBundle\Entity\home;
use Namas\ArcanaBundle\Entity\homestead;
use Namas\ArcanaBundle\Entity\land;
use Namas\ArcanaBundle\Entity\image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        //image::initImages();

        /** @var  ParameterBag $bag */
        $bag = $this->get('request')->request;
        $em = $this->getDoctrine()->getManager();
        if(isset($_POST['submit']) && $bag->get('submit') == 'search' ){
            $type = $bag->get('type');
            $operation = (in_array($type,['apartment','commercial']))?" AND p.operation = '".$bag->get('operation')."' ":'';
            $floor = (in_array($type,['apartment','commercial']))?' AND (p.flatsnumber BETWEEN '.$bag->get('min-flat').' AND '.$bag->get('max-flat').') ':'';
            $price = ' AND (p.price BETWEEN '.$bag->get('min-cost').' AND '.$bag->get('max-cost').') ';
            $city = " s.id = '".$bag->get('city')."' ";
            $country = " AND  c.id = '".$bag->get('country')."' ";
            $sql = 'SELECT p FROM  \Namas\ArcanaBundle\Entity\\'.$type.' p JOIN p.city s JOIN p.country c WHERE '.$city.'  '.$price.'  '.$operation.'  '.$floor.'  '.$country;
            $entities = $em->createQuery($sql)->getResult();

            $view = $this->chooseEntitiesView($type);
        }else{
            $repository = $em->getRepository('Namas\ArcanaBundle\Entity\apartment');
            $entities = $repository->findBy([]);//TODO
            $view = 'ArcanaBundle:Default:index.html.twig';
            $type = 'apartment';
        }

        $paginator  = $this->get('knp_paginator');
        /** @var \Knp\Component\Pager\Pagination\PaginationInterface $pagination */
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );
        $pagination->setTemplate('KnpPaginatorBundle:Pagination:bootstrap-b.html.twig');
        $path = [];
        /** @var apartment $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/object/'.$images->first()->getPath();
            }

        }
        unset($p);


        return $this->render($view, array('entities' => $pagination, 'path' => $path, 'type' => $type));

    }

    /**
     * @param $type
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function viewAction($type,$id){

        $view = $this->chooseEntityView($type);
        $class = 'Namas\ArcanaBundle\Entity\\'.$type;
        $entity = $this->getDoctrine()->getManager()->getRepository($class)->find($id);
        $images = [];
        $sliders = [];
        $carousels = [];
        /** @var image $image */
        foreach($entity->getImages()  as $image){
            $images[] = '/uploads/images/'.$image->getPath();
            $sliders[] = '/uploads/images/slider/'.$image->getPath();
            $carousels[] = '/uploads/images/carousel/'.$image->getPath();

        }
        unset($image);
        return $this->render(
            $view,
            [
                'entity' => $entity,
                'images' => $images,
                'type' => $type,
                'sliders' => $sliders,
                'carousels' => $carousels
            ]
        );


    }

    /**
     * @param $type
     * @param $operation
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function sectorAction($type, $operation){

        $condition = (in_array($type,['apartment','commercial']))?['operation'=>$operation]:[];
        $entities = $this->getDoctrine()->getManager()->getRepository('\Namas\ArcanaBundle\Entity\\'.$type)->findBy($condition);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );
        $pagination->setTemplate('KnpPaginatorBundle:Pagination:bootstrap-b.html.twig');
        $path = [];
        /** @var apartment $p */
        foreach($pagination  as $p){
            $images = $p->getImages();
            if($images->count() > 0){
                $path[$p->getId()] = '/uploads/images/'.$images->first()->getPath();
            }

        }
        unset($p);

        $view = $this->chooseEntitiesView($type);
        return $this->render($view, array('entities' => $pagination, 'type' => $type, 'path' => $path));

    }


    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function chooseEntitiesView($type)
    {

        switch($type){
            case 'apartment':
                $view = 'ArcanaBundle:Default:apartments.html.twig';
                break;
            case 'forest':
                $view = 'ArcanaBundle:Default:forests.html.twig';
                break;
            case 'home':
                $view = 'ArcanaBundle:Default:homes.html.twig';
                break;
            case 'homestead':
                $view = 'ArcanaBundle:Default:homesteads.html.twig';
                break;
            case 'land':
                $view = 'ArcanaBundle:Default:lands.html.twig';
                break;
            case 'commercial':
                $view = 'ArcanaBundle:Default:commerces.html.twig';
                break;
            default:
                throw new \Exception('type undefined');

        }
        return $view;
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function chooseEntityView($type)
    {
        switch($type){
            case 'apartment':
                $view = 'ArcanaBundle:Default:apartment.html.twig';
                break;
            case 'forest':
                $view = 'ArcanaBundle:Default:forest.html.twig';
                break;
            case 'home':
                $view = 'ArcanaBundle:Default:home.html.twig';
                break;
            case 'homestead':
                $view = 'ArcanaBundle:Default:homestead.html.twig';
                break;
            case 'land':
                $view = 'ArcanaBundle:Default:land.html.twig';
                break;
            case 'commercial':
                $view = 'ArcanaBundle:Default:commerce.html.twig';
                break;
            default:
                throw new \Exception('type undefined');

        }
        return $view;
    }

    /**
     * Creates a new forest entity.
     *
     *
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function feedbackAction(Request $request)
    {
        $entity = new post();

        $entity->setName($request->get('name'));
        $entity->setText($request->get('text'));
        $entity->setPhone($request->get('phone'));
        $entity->setPath($request->get('path'));
        $entity->setEmail($request->get('email'));
        $result =  $this->post($entity);
        if($result){
            $response = json_encode(['res' => 'ok']);
            return new Response($response);
        }
        $response = json_encode(['res' => 'error']);
        return new Response($response);

    }

    private function post(post $entity){
        $text = $entity->getText();
        $email = $entity->getEmail();
        $name = $entity->getName();
        $phone = $entity->getPhone();
        $path = $entity->getPath();

        $text="
        От кого:     $name\n
        Телефон:     $phone\n
        E-mail:      $email\n
        Со страницы: $path\n
        Сообщение:    $text\n
        ";


        $subj = "письмо с сайта ".' телефон - '.$phone.' имя - '.$name;
        $subj = '=?utf-8?B?'.base64_encode($subj).'?=';
        $res = true;
/*
        $res = mail(
            //'info@akmenesnekilnojamasturtas.lt',
            'mskorok@yandex.ru',
            $subj,
            $text
        );
*/
        if($res){
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return true;
        }
        return false;



    }

}
