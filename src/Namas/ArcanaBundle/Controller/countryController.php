<?php

namespace Namas\ArcanaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\country;
use Namas\ArcanaBundle\Form\countryType;

/**
 * country controller.
 *
 * @Route("/country")
 */
class countryController extends BaseController
{

    /**
     * Lists all country entities.
     *
     * @Route("/", name="country")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:country')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );

        return array(
            'entities' => $pagination,
        );
    }
    /**
     * Creates a new country entity.
     *
     * @Route("/", name="country_create")
     * @Method("POST")
     * @Template("ArcanaBundle:country:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new country();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('country_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a country entity.
     *
     * @param country $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(country $entity)
    {
        $form = $this->createForm(new countryType(), $entity, array(
            'action' => $this->generateUrl('country_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new country entity.
     *
     * @Route("/new", name="country_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new country();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a country entity.
     *
     * @Route("/{id}", name="country_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArcanaBundle:country')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find country entity.');
        }
        /** @var \Namas\ArcanaBundle\Entity\entity $entity */
        $entity = $this->fillTranslatedEntity($entity);



        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing country entity.
     *
     * @Route("/{id}/edit", name="country_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArcanaBundle:country')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find country entity.');
        }
        /** @var country $entity */
        $entity = $this->fillTranslatedEntity($entity);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a country entity.
    *
    * @param country $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(country $entity)
    {
        $form = $this->createForm(new countryType(), $entity, array(
            'action' => $this->generateUrl('country_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing country entity.
     *
     * @Route("/{id}", name="country_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:country:edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var country $entity */

        $entity = $em->getRepository('ArcanaBundle:country')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find country entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('country_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a country entity.
     *
     * @Route("/{id}", name="country_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ArcanaBundle:country')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find country entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('country'));
    }

    /**
     * Creates a form to delete a country entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('country_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
