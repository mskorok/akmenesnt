<?php

namespace Namas\ArcanaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TextController extends Controller
{

    public function indexAction($name){
        $view = 'ArcanaBundle:Text:'.$name.'.html.twig';
        return $this->render($view);
    }
}
