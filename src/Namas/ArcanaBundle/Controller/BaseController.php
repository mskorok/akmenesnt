<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 31.08.2014
 * Time: 14:29
 */


namespace Namas\ArcanaBundle\Controller;

use Namas\ArcanaBundle\Entity\entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class BaseController extends Controller{

    /**
     * Замена отсутствующих в локале значений параметров
     *
     * @param Entity $entity
     * @param array $exceptions
     *
     * @return Entity
     */
    public   function fillTranslatedEntity(entity $entity, array $exceptions = array('id','locale','actor')){
        $em = $this->getDoctrine()->getManager();
        //определяем локаль по умолчанию
        $defloc=$this->container->getParameter('kernel.default_locale');
        $loc=$this->container->get('request_stack')->getCurrentRequest()->getLocale();
        $className = get_class($entity);
        $ref = new \ReflectionClass($className);
        $classProperties = $ref->getProperties();
        $properties = array();
        foreach ($classProperties as $prop) {
            $properties[] = $prop->getName();
        }
        $id=$entity->getId();
        $obj = $em->find($className, $id);
        $repository = $em->getRepository('Gedmo\Translatable\Entity\Translation');
        $translations = $repository->findTranslations($obj);
        /* $translations contains:
        Array (
            [de_de] => Array
                (
                    [title] => my title in de
                    [content] => my content in de
                )

            [en_us] => Array
                (
                    [title] => my title in en
                    [content] => my content in en
                )
        )*/

        foreach($properties as $property){
//echo "_".$property."_";
            $method='get'.ucfirst(strtolower($property));
            //определяем наличие гетера для свойства, если нет то не делаем ничего

            if(method_exists($entity, $method) && !in_array($property, $exceptions)){

                //проверяем пустое ли свойство, если пустое то пытаемся его чем-то заполнить
                if(!isset($translations[$loc][$property]) && empty($translations[$loc][$property])){
                    //echo 1;
                    if($entity->$method($property) instanceof \Countable && count($entity->$method($property)) > 0 ){
                        //echo 5;
                        $collection = new ArrayCollection();
                        foreach($entity->$method($property) as $item){
                            if($item instanceof entity){
                                //echo  get_class($item);
                                $item = $this->fillTranslatedEntity($item);

                                $collection->add($item);
                            }


                        }
                        $method='set'.ucfirst(strtolower($property));
                        if(method_exists($entity, $method)){
                            $entity->$method($collection);
                        }
                    }else{
                        // echo 6;
                        //определяет наличие сетера для метода, если нет то не делаем ничего
                        $method='set'.ucfirst(strtolower($property));
                        if(method_exists($entity, $method)){
                            //проверяем наличие записи свойства на дефолтной локали, если нет то ищем на первой попавшейся
                            if(isset($translations[$defloc][$property]) && !empty($translations[$defloc][$property])){
                                //заполняем пустое свойство
                                $entity->$method($translations[$defloc][$property]);

                            }else{

                                foreach($translations as $transl){
                                    if(isset($transl[$property]) && !empty($transl[$property])){
                                        $entity->$method($transl[$property]);
                                        break;
                                    }
                                }
                            }

                        }
                    }



                }elseif(is_string($entity->$method($property)) && $entity->$method($property) != $translations[$loc][$property]){
                    // echo 2;
                    $method='set'.ucfirst(strtolower($property));
                    if(method_exists($entity, $method)){
                        $entity->$method($translations[$loc][$property]);
                    }



                }
                // echo 4;
            }

        }

        return $entity;

    }

}