<?php

namespace Namas\ArcanaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\city;
use Namas\ArcanaBundle\Form\cityType;

/**
 * city controller.
 *
 * @Route("/city")
 */
class cityController extends BaseController
{

    /**
     * Lists all city entities.
     *
     * @Route("/", name="city")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:city')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );

        return array(
            'entities' => $pagination,
        );
    }
    /**
     * Creates a new city entity.
     *
     * @Route("/", name="city_create")
     * @Method("POST")
     * @Template("ArcanaBundle:city:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new city();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('city_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a city entity.
     *
     * @param city $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(city $entity)
    {
        $form = $this->createForm(new cityType(), $entity, array(
            'action' => $this->generateUrl('city_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new city entity.
     *
     * @Route("/new", name="city_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new city();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a city entity.
     *
     * @Route("/{id}", name="city_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArcanaBundle:city')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find city entity.');
        }
        /** @var \Namas\ArcanaBundle\Entity\entity $entity */
        $entity = $this->fillTranslatedEntity($entity);

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing city entity.
     *
     * @Route("/{id}/edit", name="city_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var city $entity */
        $entity = $em->getRepository('ArcanaBundle:city')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find city entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a city entity.
    *
    * @param city $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(city $entity)
    {
        $form = $this->createForm(new cityType(), $entity, array(
            'action' => $this->generateUrl('city_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing city entity.
     *
     * @Route("/{id}", name="city_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:city:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var city $entity */
        $entity = $em->getRepository('ArcanaBundle:city')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find city entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('city_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a city entity.
     *
     * @Route("/{id}", name="city_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ArcanaBundle:city')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find city entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('city'));
    }

    /**
     * Creates a form to delete a city entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('city_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
