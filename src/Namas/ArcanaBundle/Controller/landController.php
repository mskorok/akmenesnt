<?php

namespace Namas\ArcanaBundle\Controller;

use Namas\ArcanaBundle\Entity\entity;
use Namas\ArcanaBundle\Entity\image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\land;
use Namas\ArcanaBundle\Form\landType;

/**
 * land controller.
 *
 * @Route("/land")
 */
class landController extends BaseController
{

    /**
     * Lists all land entities.
     *
     * @Route("/", name="land")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:land')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination
        );
    }
    /**
     * Creates a new land entity.
     *
     * @Route("/", name="land_create")
     * @Method("POST")
     * @Template("ArcanaBundle:land:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new land();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('land_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a land entity.
     *
     * @param land $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(land $entity)
    {
        $form = $this->createForm(new landType(), $entity, array(
            'action' => $this->generateUrl('land_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new land entity.
     *
     * @Route("/new", name="land_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new land();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a land entity.
     *
     * @Route("/{id}", name="land_show")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var land $entity */
        $entity = $em->getRepository('ArcanaBundle:land')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find land entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $images= $entity->getImages();
        $img = '';
        foreach($images as $image){
            /** @var string $img */
            $img .= '<img src="/uploads/images/'.$image->getPath().'" width="150" height="125" />';
        }
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img
        );
    }

    /**
     * Displays a form to edit an existing land entity.
     *
     * @Route("/{id}/edit", name="land_edit")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entity */
        $entity = $em->getRepository('ArcanaBundle:land')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find land entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a land entity.
    *
    * @param land $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(land $entity)
    {
        $form = $this->createForm(new landType(), $entity, array(
            'action' => $this->generateUrl('land_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing land entity.
     *
     * @Route("/{id}", name="land_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:land:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var entity $entity */
        $entity = $em->getRepository('ArcanaBundle:land')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find land entity.');
        }

        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('land_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a land entity.
     *
     * @Route("/{id}", name="land_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var land $entity */
            $entity = $em->getRepository('ArcanaBundle:land')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find land entity.');
            }
            $images =$entity->getImages();
            /** @var image $image */
            foreach($images as $image){
                $em->remove($image);
                $em->flush();
            }
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('land'));
    }

    /**
     * Creates a form to delete a land entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('land_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $entity = new land();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-form')
        );

    }
}
