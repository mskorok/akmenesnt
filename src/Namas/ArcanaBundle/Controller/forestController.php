<?php

namespace Namas\ArcanaBundle\Controller;

use Namas\ArcanaBundle\Entity\image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Namas\ArcanaBundle\Entity\forest;
use Namas\ArcanaBundle\Form\forestType;

/**
 * forest controller.
 *
 * @Route("/forest")
 */
class forestController extends BaseController
{

    /**
     * Lists all forest entities.
     *
     * @Route("/", name="forest")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ArcanaBundle:forest')->findAll();

        $filledEntities = array();
        foreach($entities as $entity){
            $filledEntities[] = $this->fillTranslatedEntity($entity);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filledEntities,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return array(
            'entities' => $filledEntities,
            'pagination'  => $pagination
        );
    }
    /**
     * Creates a new forest entity.
     *
     * @Route("/", name="forest_create")
     * @Method("POST")
     * @Template("ArcanaBundle:forest:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new forest();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('forest_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a forest entity.
     *
     * @param forest $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(forest $entity)
    {
        $form = $this->createForm(new forestType(), $entity, array(
            'action' => $this->generateUrl('forest_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new forest entity.
     *
     * @Route("/new", name="forest_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new forest();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a forest entity.
     *
     * @Route("/{id}", name="forest_show")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var forest $entity */
        $entity = $em->getRepository('ArcanaBundle:forest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forest entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $images= $entity->getImages();
        $img = '';
        foreach($images as $image){
            $img .= '<img src="/uploads/images/'.$image->getPath().'" width="150" height="125" />';
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'image'       => $img
        );
    }

    /**
     * Displays a form to edit an existing forest entity.
     *
     * @Route("/{id}/edit", name="forest_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var forest $entity */
        $entity = $em->getRepository('ArcanaBundle:forest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forest entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a forest entity.
    *
    * @param forest $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(forest $entity)
    {
        $form = $this->createForm(new forestType(), $entity, array(
            'action' => $this->generateUrl('forest_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing forest entity.
     *
     * @Route("/{id}", name="forest_update")
     * @Method("PUT")
     * @Template("ArcanaBundle:forest:edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var forest $entity */
        $entity = $em->getRepository('ArcanaBundle:forest')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find forest entity.');
        }
        $entity = $this->fillTranslatedEntity($entity);
        $entity->setLocale($request->getLocale());

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('forest_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a forest entity.
     *
     * @Route("/{id}", name="forest_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
             /** @var forest $entity */
            $entity = $em->getRepository('ArcanaBundle:forest')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find forest entity.');
            }
            $images =$entity->getImages();
            /** @var image $image */
            foreach($images as $image){
                $em->remove($image);
                $em->flush();
            }
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('forest'));
    }

    /**
     * Creates a form to delete a forest entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('forest_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Ištrinti'), 'attr' =>array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public  function delAction($id)
    {
        $deleteForm = $this->createDeleteForm($id);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $deleteForm->createView(), 'class'=> 'del')
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction()
    {
        $entity = new forest();
        $form   = $this->createCreateForm($entity);
        return $this->render(
            'ArcanaBundle::form.html.twig',
            array('form' => $form->createView(), 'class'=> 'modal-form')
        );

    }
}
