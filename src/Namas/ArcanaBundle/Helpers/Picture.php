<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 10.01.2015
 * Time: 16:20
 */

namespace Namas\ArcanaBundle\Helpers;


class Picture {
    private $file;

    protected $image;
    protected $type;
    protected $width;
    protected $height;


    public function __construct($file) {
        $this->file = $file;
        $info = getimagesize($this->file);
        $this->width = $info[0];
        $this->height = $info[1];
        switch($info[2]) {
            case 1: $this->type = 'gif'; break;//1: IMAGETYPE_GIF
            case 2: $this->type = 'jpeg'; break;//2: IMAGETYPE_JPEG
            case 3: $this->type = 'png'; break;//3: IMAGETYPE_PNG
            case 4: $this->type = 'swf'; break;//4: IMAGETYPE_SWF
            case 5: $this->type = 'psd'; break;//5: IMAGETYPE_PSD
            case 6: $this->type = 'bmp'; break;//6: IMAGETYPE_BMP
            case 7: $this->type = 'tiffi'; break;//7: IMAGETYPE_TIFF_II (порядок байт intel)
            case 8: $this->type = 'tiffm'; break;//8: IMAGETYPE_TIFF_MM (порядок байт motorola)
            case 9: $this->type = 'jpc'; break;//9: IMAGETYPE_JPC
            case 10: $this->type = 'jp2'; break;//10: IMAGETYPE_JP2
            case 11: $this->type = 'jpx'; break;//11: IMAGETYPE_JPX
            case 12: $this->type = 'jb2'; break;//12: IMAGETYPE_JB2
            case 13: $this->type = 'swc'; break;//13: IMAGETYPE_SWC
            case 14: $this->type = 'iff'; break;//14: IMAGETYPE_IFF
            case 15: $this->type = 'wbmp'; break;//15: IMAGETYPE_WBMP
            case 16: $this->type = 'xbm'; break;//16: IMAGETYPE_XBM
            case 17: $this->type = 'ico'; break;//17: IMAGETYPE_ICO
            default: $this->type = ''; break;
        }
        $this->createImage();
    }

    private function createImage() {
        switch($this->type) {
            case 'gif':
                $this->image = imagecreatefromgif($this->file);
                break;
            case 'jpeg':
                $this->image = imagecreatefromjpeg($this->file);
                break;
            case 'png':
                $this->image = imagecreatefrompng($this->file);
                break;
            default:
                $this->image = null;
        }
    }

    public function autoImageResize($new_w, $new_h) {
        $difference_w = 0;
        $difference_h = 0;
        if($this->width < $new_w && $this->height < $new_h) {
            $this->imageResize($this->width, $this->height);
        }
        else {
            if($this->width > $new_w) {
                $difference_w = $this->width - $new_w;
            }
            if($this->height > $new_h) {
                $difference_h = $this->height - $new_h;
            }
            if($difference_w > $difference_h) {
                $this->imageResizeWidth($new_w);
            }
            elseif($difference_w < $difference_h) {
                $this->imageResizeHeight($new_h);
            }
            else {
                $this->imageResize($new_w, $new_h);
            }
        }
    }

    public function percentImageReduce($percent) {
        $new_w = $this->width * $percent / 100;
        $new_h = $this->height * $percent / 100;
        $this->imageResize($new_w, $new_h);
    }

    public function imageResizeWidth($new_w) {
        $new_h = $this->height * ($new_w / $this->width);
        $this->imageResize($new_w, $new_h);
    }

    public function imageResizeHeight($new_h) {
        $new_w = $this->width * ($new_h / $this->height);
        $this->imageResize($new_w, $new_h);
    }

    public function imageResize($new_w, $new_h) {
        if($this->image === null){
            return false;
        }
        $new_image = imagecreatetruecolor($new_w, $new_h);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $new_w, $new_h, $this->width, $this->height);
        $this->width = $new_w;
        $this->height = $new_h;
        $this->image = $new_image;
        return true;
    }

    public function imageSave($type='jpeg', $file=NULL, $compress=100, $reSave = false, $permission=null) {
        if($reSave){
            $file = $this->file;
        }
        if($file == NULL) {
            switch($type) {
                case 'gif': header("Content-type: image/gif"); break;
                case 'jpeg': header("Content-type: image/jpeg"); break;
                case 'png': header("Content-type: image/png"); break;
            }
        }
        switch($type) {
            case 'gif': imagegif($this->image, $file); break;
            case 'jpeg': imagejpeg($this->image, $file, $compress); break;
            case 'png': imagepng($this->image, $file); break;
        }
        if($permission) {
            chmod($file, $permission);
        }
    }

    public function imageOut() {
        imagedestroy($this->image);
    }

    public function __destruct() {

    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }


    

}