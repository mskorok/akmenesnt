<?php

namespace Namas\ArcanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class forestType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('country')
            ->add('square')
            ->add('address')
            ->add('price')
            ->add('description')
            ->add('reserved', 'choice', array(
                'choices'   => array('1' => 'active', '0' => 'reserved'),
                'preferred_choices' => array('1'),
                'required'  => true,
            ))
            ->add('city')
            ->add('isActive', 'choice', array(
                'choices'   => array('1' => 'active', '0' => 'archive'),
                'required'  => true,
            ))
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Namas\ArcanaBundle\Entity\forest'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'namas_arcanabundle_forest';
    }
}
