<?php

namespace Namas\ArcanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class apartmentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('country')
            ->add('flatsnumber')
            ->add('floor')
            ->add('storeysnumber')
            ->add('square')
            ->add('address')
            ->add('price')
            ->add('description')
            ->add('reserved', 'choice', array(
                'choices'   => array('1' => 'active', '0' => 'reserved'),
                'preferred_choices' => array('1'),
                'required'  => true,
            ))
            ->add('operation', 'choice', array(
                'choices'   => array('sell' => 'sell', 'rent' => 'rent'),
                'required'  => true,
            ))
            ->add('city')
            ->add('isActive', 'choice', array(
                'choices'   => array('1' => 'active', '0' => 'archive'),
                'required'  => true,
            ))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Namas\ArcanaBundle\Entity\apartment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'namas_arcanabundle_apartment';
    }
}
