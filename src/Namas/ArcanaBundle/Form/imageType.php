<?php

namespace Namas\ArcanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class imageType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('owner')
            ->add('apartment')
            ->add('commercial')
            ->add('forest')
            ->add('homestead')
            ->add('home')
            ->add('land')
            ->add('description')
            ->add('image')

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Namas\ArcanaBundle\Entity\image'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'namas_arcanabundle_image';
    }
}
