<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 14.05.2014
 * Time: 9:14
 */

namespace Acme\Bundle\ExBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
class LocaleExType extends AbstractType{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'russian' => 'ru',
                'english' => 'en',
                'lithuanian' => 'lt',
                'ukrainian' => 'ua',
            ),
            'required'    => true,
        ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'locale_ex';
    }
} 