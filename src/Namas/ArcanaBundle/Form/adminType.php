<?php

namespace Namas\ArcanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use  FOS\UserBundle\Model\UserInterface;

class adminType extends AbstractType
{
    const GENDER_FEMALE  = 'f';
    const GENDER_MALE    = 'm';
    const GENDER_MAN     = 'm'; // @deprecated
    const GENDER_UNKNOWN = 'u';
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('position')
            ->add('username','fos_user_username')
            ->add('usernameCanonical')
            ->add('email', 'email')
            ->add('emailCanonical', 'email')
            ->add('enabled', 'choice', array(
                'choices'   => array(
                  '1'   => 'Yes',
                  '0' => 'No',
                ),
                'expanded' => true,
                'multiple' => false,
                'preferred_choices' => array('1'),
                )
            )
            ->add('salt')
            ->add('password', 'password')
            ->add('locked','choice', array('choices'   => array(
                    '1'   => 'Yes',
                    '0' => 'No',
                ),
                'expanded' => true,
                'multiple' => false,
                'preferred_choices' => array('1'),
                'required' => false))
            ->add('expired', null, array('required' => false))// todo   записать в сеттер
            ->add('expiresAt', 'datetime', array('required' => false))
            ->add('confirmationToken', null,  array('required' => false))->add('passwordRequestedAt', 'datetime', array('required' => false))
            ->add('roles', 'sonata_security_roles', array(
                'label'    => 'form.label_roles',
                'expanded' => true,
                'multiple' => true,
                'required' => false
                )
            )
            ->add('credentialsExpired', null, array('required' => false))//todo записать в сеттер
            ->add('credentialsExpireAt', 'datetime', array('required' => false))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Namas\ArcanaBundle\Entity\admin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'namas_arcanabundle_admin';
    }
}
