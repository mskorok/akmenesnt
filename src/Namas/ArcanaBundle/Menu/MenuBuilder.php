<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22.05.2014
 * Time: 8:15
 */

namespace Namas\ArcanaBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
class MenuBuilder extends ContainerAware{

    /**
     *
     *
     * <ul class="bottom-nav-block">
     *     <li class="inner-li-bottom-nav-left">
     *         <a href="{{ path('actor') }}" class="bottom-nav-link" >{% trans %}Actor{% endtrans %}</a>
     *     </li>
     *     <li class="inner-li-bottom-nav">
     *         <a href="{{ path('author') }}" class="bottom-nav-link">{% trans %}Author{% endtrans %}</a>
     *     </li>
     *     <li class="inner-li-bottom-nav-right">
     *         <a href="{{ path('tag') }}" class="bottom-nav-link">{% trans %}Tag{% endtrans %}</a>
     *     </li>
     * </ul>
     *
     *
     *
     * @param FactoryInterface $factory
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function bottomMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');


        $apartment       = $this->container->get('translator')->trans('Apartment');
        $commercial      = $this->container->get('translator')->trans('Commercial');
        $forest          = $this->container->get('translator')->trans('Forest');
        $home            = $this->container->get('translator')->trans('Home');
        $homestead       = $this->container->get('translator')->trans('Homestead');
        $land            = $this->container->get('translator')->trans('Land');
        $owner           = $this->container->get('translator')->trans('Owner');
        $image           = $this->container->get('translator')->trans('Image');
        $city            = $this->container->get('translator')->trans('City');
        $country         = $this->container->get('translator')->trans('Country');


        $menu->addChild($apartment, array('route' => 'apartment'));
        $menu->addChild($commercial, array('route' => 'commercial'));
        $menu->addChild($forest, array('route' => 'forest'));
        $menu->addChild($home, array('route' => 'home'));
        $menu->addChild($homestead, array('route' => 'homestead'));
        $menu->addChild($land, array('route' => 'land'));
        $menu->addChild($owner, array('route' => 'owner'));
        $menu->addChild($image, array('route' => 'image'));
        $menu->addChild($city, array('route' => 'city'));
        $menu->addChild($country, array('route' => 'country'));
        // ... add more children
        //$menu['Actor']->setLinkAttribute('class', 'external-link');
        //$menu['Actor']->setLabelAttribute('class', 'no-link-span');
        $menu->setChildrenAttribute('class', 'bottom-nav-block');
        $menu[$apartment]->setAttribute('class', 'inner-li-bottom-nav-left')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$commercial]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$forest]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$home]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$homestead]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$land]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$owner]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$image]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$city]->setAttribute('class', 'inner-li-bottom-nav')->setLinkAttribute('class', 'bottom-nav-link');
        $menu[$country]->setAttribute('class', 'inner-li-bottom-nav-right')->setLinkAttribute('class', 'bottom-nav-link');
        return $menu;
    }


    /**
     *
     * <ul class="top-nav-block">
    <li class="inner-li-top-nav-left">
    <a href="{{ path('film') }}" class="top-nav-link" >{% trans %}Film{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav">
    <a href="{{ path('book') }}" class="top-nav-link">{% trans %}Book{% endtrans %}</a>
    </li>
    <li class="inner-li-top-nav-right">
    <a href="{{ path('sonata_admin_dashboard') }}" class="top-nav-link">Sonata</a>
    </li>
    </ul>
     *
     * @param FactoryInterface $factory
     * @param array $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function topMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');


        $apartment       = $this->container->get('translator')->trans('Apartment');
        $commercial      = $this->container->get('translator')->trans('Commercial');
        $forest          = $this->container->get('translator')->trans('Forest');
        $home            = $this->container->get('translator')->trans('Home');
        $homestead       = $this->container->get('translator')->trans('Homestead');
        $land            = $this->container->get('translator')->trans('Land');
        $image           = $this->container->get('translator')->trans('Image');



        $menu->addChild($apartment, array('route' => 'apartment'));
        $menu->addChild($commercial, array('route' => 'commercial'));
        $menu->addChild($forest, array('route' => 'forest'));
        $menu->addChild($home, array('route' => 'home'));
        $menu->addChild($homestead, array('route' => 'homestead'));
        $menu->addChild($land, array('route' => 'land'));
        $menu->addChild($image, array('route' => 'image'));
        // ... add more children
        //$menu['Actor']->setLinkAttribute('class', 'external-link');
        //$menu['Actor']->setLabelAttribute('class', 'no-link-span');
        $menu->setChildrenAttribute('class', 'top-nav-block');
        $menu[$apartment]->setAttribute('class', 'inner-li-top-nav-left')->setLinkAttribute('class', 'top-nav-link');
        $menu[$commercial]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$forest]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$home]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$homestead]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$land]->setAttribute('class', 'inner-li-top-nav')->setLinkAttribute('class', 'top-nav-link');
        $menu[$image]->setAttribute('class', 'inner-li-top-nav-right')->setLinkAttribute('class', 'top-nav-link');

        return $menu;
    }
} 