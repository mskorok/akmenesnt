<?php

namespace Namas\ArcanaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * forest
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Namas\ArcanaBundle\Entity\forestRepository")
 */
class forest extends entity implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected  $title;

    /**
     * @var string
     *
     *
     * @ORM\ManyToOne(targetEntity="country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id",  nullable=true)
     *
     *
     */
    protected  $country;

    /**
     * @var integer
     *
     * @ORM\Column(name="square", type="float", nullable=TRUE, columnDefinition="FLOAT(11,2)")
     */
    protected  $square;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="address", type="string", length=255)
     */
    protected  $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="float", nullable=TRUE, columnDefinition="FLOAT(11,2)")
     */
    protected  $price;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text",   nullable=true)
     */
    protected  $description;
    /**
     * @var integer
     *
     * @ORM\Column(name="reserved", type="integer")
     */
    protected  $reserved;
    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="city")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id",  nullable=true)
     */
    protected  $city;
    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer")
     */
    protected  $isActive;

    /**
     * @var string
     *
     *  @Gedmo\Locale
     */
    protected $locale;

    /**
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="image", mappedBy="forest")
     * )
     */
    protected  $images;

    public function __construct() {
        $this->images = new ArrayCollection();
    }

    public function __toString(){

        return $this->getTitle();
    }

    /**
     * @return int
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * @param int $reserved
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set square
     *
     * @param integer $square
     * @return forest
     */
    public function setSquare($square)
    {
        $this->square = $square;

        return $this;
    }

    /**
     * Get square
     *
     * @return integer 
     */
    public function getSquare()
    {
        return $this->square;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return forest
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }



    /**
     * Set description
     *
     * @param string $description
     * @return forest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }



}
