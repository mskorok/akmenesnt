<?php

namespace Namas\ArcanaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;
/**
 * admin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Namas\ArcanaBundle\Entity\adminRepository")
 */
class admin extends User
{

    public function __construct()
    {
        parent::__construct();

        $this->roles = array(static::ROLE_DEFAULT);

    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255,   nullable=true)
     */
    protected  $position;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return admin
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
