<?php

namespace Namas\ArcanaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * commercial
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Namas\ArcanaBundle\Entity\commercialRepository")
 */
class commercial extends entity implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected  $title;

    /**
     * @var string
     *
     *
     * @ORM\ManyToOne(targetEntity="country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id",  nullable=true)
     *
     *
     */
    protected  $country;

    /**
     * @var integer
     *
     * @ORM\Column(name="flatsnumber", type="integer",   nullable=true)
     */
    protected  $flatsnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="integer",   nullable=true)
     */
    protected  $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="storeysnumber", type="integer",  nullable=true)
     */
    protected  $storeysnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="square", type="float", nullable=TRUE, columnDefinition="FLOAT(11,2)")
     */
    protected  $square;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="address", type="string", length=255)
     */
    protected  $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="float", nullable=TRUE, columnDefinition="FLOAT(11,2)")
     */
    protected  $price;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text",   nullable=true)
     */
    protected  $description;
    /**
     * @var integer
     *
     * @ORM\Column(name="reserved", type="integer")
     */
    protected  $reserved;
    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="operation", type="string", length=255, options={"default":"sell"} )
     */
    protected  $operation;
    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="city")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id",  nullable=true)
     */
    protected  $city;
    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer")
     */
    protected  $isActive;


    /**
     * @var string
     *
     *  @Gedmo\Locale
     */
    protected $locale;

    /**
     *  @var ArrayCollection
     *
     *  @ORM\OneToMany(targetEntity="image", mappedBy="commercial")
     * )
     */
    protected  $images;

    public function __construct() {
        $this->images = new ArrayCollection();
    }

    public function __toString(){

        return $this->getTitle();
    }

    /**
     * @return int
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * @param int $reserved
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set flatsnumber
     *
     * @param integer $flatsnumber
     * @return commercial
     */
    public function setFlatsnumber($flatsnumber)
    {
        $this->flatsnumber = $flatsnumber;

        return $this;
    }

    /**
     * Get flatsnumber
     *
     * @return integer 
     */
    public function getFlatsnumber()
    {
        return $this->flatsnumber;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     * @return commercial
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer 
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set storeysnumber
     *
     * @param integer $storeysnumber
     * @return commercial
     */
    public function setStoreysnumber($storeysnumber)
    {
        $this->storeysnumber = $storeysnumber;

        return $this;
    }

    /**
     * Get storeysnumber
     *
     * @return integer 
     */
    public function getStoreysnumber()
    {
        return $this->storeysnumber;
    }

    /**
     * Set square
     *
     * @param integer $square
     * @return commercial
     */
    public function setSquare($square)
    {
        $this->square = $square;

        return $this;
    }

    /**
     * Get square
     *
     * @return integer 
     */
    public function getSquare()
    {
        return $this->square;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return commercial
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return commercial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }



}
