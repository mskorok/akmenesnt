<?php

namespace Namas\ArcanaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert; //- See more at: http://symfony2.ylly.fr/upload-file-with-doctrine-in-symfony2-jordscream/#sthash.CuPC1SEr.dpuf
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Namas\ArcanaBundle\Helpers;





/**
 * image
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Namas\ArcanaBundle\Entity\imageRepository")
 */
class image extends entity implements Translatable
{


    private $temp;
    private static $images = [
        'object' => ['width' => 250, 'height' => 180, 'compress' => 100],
        'slider' => ['width' => 600, 'height' => 450, 'compress' => 100],
        'carousel' => ['width' => 206, 'height' => 153, 'compress' => 100],
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected  $title;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="owner", inversedBy="images")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     *
     *
     */
    protected  $owner;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="apartment", inversedBy="images")
     * @ORM\JoinColumn(name="apartment_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $apartment;
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="commercial", inversedBy="images")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $commercial;
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="forest", inversedBy="images")
     * @ORM\JoinColumn(name="forest_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $forest;
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="home", inversedBy="images")
     * @ORM\JoinColumn(name="homet_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $home;
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="homestead", inversedBy="images")
     * @ORM\JoinColumn(name="homestead_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $homestead;
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="land", inversedBy="images")
     * @ORM\JoinColumn(name="land_id", referencedColumnName="id", nullable=true)
     *
     *
     */
    protected  $land;



    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text")
     */
    protected  $description;

    /**
     * @var UploadedFile
     *
     *
     * @Assert\File( maxSize = "6024k", mimeTypesMessage = "Please upload a valid Image")
     *
     *
     */
    protected  $image;

    /**
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    protected  $path;
    /**
     * @var string
     *
     *  @Gedmo\Locale
     */
    protected $locale;


    /******************************************************************************************************************
     * FUNCTIONS
     *****************************************************************************************************************/

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    /**
     *  create additional images with custom parameters
     */
    public function createImages(){
        $picture = new Helpers\Picture($this->getAbsolutePath());
        foreach(self::getImages() as $key => $size){
            $path = self::getCustomRootUploadDir($key).'/'.$this->path;
            $picture->imageSave('jpeg', $path, $size['compress']);
            $pic = new Helpers\Picture($path);
            $new_w = $size['width'];
            $new_h = $size['height'];
            $pic->autoImageResize($new_w, $new_h);
            $pic->imageSave('jpeg', $path, $size['compress']);
            unset($pic);
        }

    }


    public static function deleteImages($imagePath){
        foreach(self::getImages() as $key => $size){
            $path = @self::getCustomRootUploadDir($key).'/'.$imagePath;
            @unlink($path);
        }
    }

    public static function initImages(){

        $dir = dir(@self::getUploadRootDir());
        while(($file = $dir->read()) !== false)
        {
            if(!in_array($file,['.','..','carousel','object','slider'])){
                $ext = pathinfo($file, PATHINFO_EXTENSION );
                $filePath = @self::getUploadRootDir().'/'.$file;
                $picture = new Helpers\Picture($filePath);

                foreach(self::getImages() as $key => $item){
                    $path = @self::getCustomRootUploadDir($key).'/'.$file;
                    if(in_array($ext,['jpeg','gif','png'])){
                        $picture->imageSave('jpeg', $path, $item['compress']);
                        $pic = new Helpers\Picture($path);
                        $new_w = $item['width'];
                        $new_h = $item['height'];
                        $pic->autoImageResize($new_w, $new_h);
                        $pic->imageSave('jpeg', $path, $item['compress']);
                        unset($pic);
                    }else{
                        copy($filePath,$path);
                    }

                }
            }

        }
        $dir->close();
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../public_html/'.self::getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images';
    }

    protected static function getCustomRootUploadDir($name)
    {
        return @self::getUploadRootDir().'/'.$name;
    }



    public function __toString(){

        return $this->getTitle();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImage(UploadedFile $file = null)
    {
        $this->image = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getImage()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getImage()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getImage()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getImage()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            @unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->image = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
    }



    /******************************************************************************************************************
     * GETTERS AND SETTERS
     *****************************************************************************************************************/



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return image
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return image
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Get image
     *
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param int $apartment
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;
    }

    /**
     * @return int
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * @param int $commercial
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;
    }

    /**
     * @return int
     */
    public function getForest()
    {
        return $this->forest;
    }

    /**
     * @param int $forest
     */
    public function setForest($forest)
    {
        $this->forest = $forest;
    }

    /**
     * @return int
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param int $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return int
     */
    public function getHomestead()
    {
        return $this->homestead;
    }

    /**
     * @param int $homestead
     */
    public function setHomestead($homestead)
    {
        $this->homestead = $homestead;
    }

    /**
     * @return int
     */
    public function getLand()
    {
        return $this->land;
    }

    /**
     * @param int $land
     */
    public function setLand($land)
    {
        $this->land = $land;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param mixed $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * @return array
     */
    public static function getImages()
    {
        return self::$images;
    }

    /**
     * @param array $images
     */
    public static function setImages($images)
    {
        self::$images = $images;
    }






}
