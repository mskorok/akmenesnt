<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 21.05.2014
 * Time: 12:09
 */

namespace Namas\ArcanaBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
class entity implements Translatable{

    protected $id;
    protected $locale;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale) {
        $this->locale = $locale;
    }


} 