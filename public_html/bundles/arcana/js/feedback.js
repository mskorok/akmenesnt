/**
 * Created by Mike on 03.09.2014.
 */

$(document).ready(
    function() {
        $(".pochta-mail-en").click(function () {
            $('.fb').show();
        });
        $(".close").click(function () {
            $('.fb').hide();
        });
        var tar = window.location.pathname;
        tar = tar.substr(0,3) + '/feedback';

        $('#sbm').click(function(e){

            //var tt = $('#feedback-captcha-code').val();
            var FeedbackForm_path = $('#feedback-path').val();
            var FeedbackForm_name = $('#feedback-name').val();
            var FeedbackForm_email = $('#feedback-email').val();
            var FeedbackForm_phone = $('#feedback-phone').val();
            var FeedbackForm_text = $('#feedback-text').val();

            var fdata = {'path':FeedbackForm_path,
                'name':FeedbackForm_name,
                'email':FeedbackForm_email,
                'phone':FeedbackForm_phone,
                'text':FeedbackForm_text


            };

            e.stopPropagation();




                jQuery.ajax({
                    url: tar,
                    type: "POST",
                    dataType: "json",
                    cache: false,
                    async: false,
                    data:fdata,
                    success: function(data) {

                        var res = (data.res)?data.res:'error';
                        requestHandler(res);
                    },
                    error:function(jqXHR, exception){
                        $('.fb').hide();
                        alert(exception);
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                        return false;
                    }
                });

                return false;



        });


    }
);
function requestHandler(res){
    $('.fb').hide();
    $('#feedback-text').val('');
    $('#feedback-name').val('');
    $('#feedback-phone').val('');
    $('#feedback-email').val('');
    if(res == 'ok'){
        $('#feedback-success').show();
    }
    if(res == 'error'){
        $('#feedback-error').show();
    }
    setTimeout(function(){
        $('#feedback-error').hide();
        $('#feedback-success').hide();
    }, 5000);
}