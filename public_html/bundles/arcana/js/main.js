/**
 * Created by Mike on 18.10.2014.
 */
$(document).ready(
    function() {
        var ownerEl = $('select#namas_arcanabundle_image_owner');
        var owner = ownerEl.val();
        hideAll(owner);
        ownerEl.change(
            function(e){
                var value = ownerEl.val();
                if(value == 1 ){
                    hideAll(1);
                    $('#namas_arcanabundle_image_apartment').show();
                    $('label[for=namas_arcanabundle_image_apartment]').show();

                }
                if(value == 2 ){
                    hideAll(2);
                    $('#namas_arcanabundle_image_home').show();
                    $('label[for=namas_arcanabundle_image_home]').show();

                }

                if(value == 3 ){
                    hideAll(3);
                    $('#namas_arcanabundle_image_commercial').show();
                    $('label[for=namas_arcanabundle_image_commercial]').show();

                }

                if(value == 4 ){
                    hideAll(4);
                    $('#namas_arcanabundle_image_forest').show();
                    $('label[for=namas_arcanabundle_image_forest]').show();

                }
                if(value == 5 ){
                    hideAll(5);
                    $('#namas_arcanabundle_image_homestead').show();
                    $('label[for=namas_arcanabundle_image_homestead]').show();

                }
                if(value == 6 ){
                    hideAll(6);
                    $('#namas_arcanabundle_image_land').show();
                    $('label[for=namas_arcanabundle_image_land]').show();

                }


            }
        );




    }
);

    function hideAll(value){
        if(value != 1){
            $('#namas_arcanabundle_image_apartment').hide();
            $('label[for=namas_arcanabundle_image_apartment]').hide();
        }
        if(value != 3){
            $('#namas_arcanabundle_image_commercial').hide();
            $('label[for=namas_arcanabundle_image_commercial]').hide();
        }
        if(value != 4){
            $('#namas_arcanabundle_image_forest').hide();
            $('label[for=namas_arcanabundle_image_forest]').hide();
        }
        if(value != 2){
            $('#namas_arcanabundle_image_home').hide();
            $('label[for=namas_arcanabundle_image_home]').hide();
        }
        if(value != 5){
            $('#namas_arcanabundle_image_homestead').hide();
            $('label[for=namas_arcanabundle_image_homestead]').hide();
        }
        if(value != 6){
            $('#namas_arcanabundle_image_land').hide();
            $('label[for=namas_arcanabundle_image_land]').hide();
        }
    }
